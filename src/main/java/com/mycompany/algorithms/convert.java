/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.algorithms;

import java.util.Scanner;

/**
 *
 * @author admin
 */
public class convert {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        String input = kb.next(); //รับค่า intput
        System.out.print(convrt(input)); // เรียก method
    }

    private static int convrt(String s) {
        int result = 0;
        for (int i = 0; i < s.length(); i++) { // for loop วนตั้งแต่หลักหน่วย
            if (s.charAt(i) == '1') {
                result += Math.pow(10, s.length() - 1 - i) * 1;
            } else if (s.charAt(i) == '2') {
                result += Math.pow(10, s.length() - 1 - i) * 2;
            } else if (s.charAt(i) == '3') {
                result += Math.pow(10, s.length() - 1 - i) * 3;
            } else if (s.charAt(i) == '4') {
                result += Math.pow(10, s.length() - 1 - i) * 4;
            } else if (s.charAt(i) == '5') {
                result += Math.pow(10, s.length() - 1 - i) * 5;
            } else if (s.charAt(i) == '6') {
                result += Math.pow(10, s.length() - 1 - i) * 6;
            } else if (s.charAt(i) == '7') {
                result += Math.pow(10, s.length() - 1 - i) * 7;
            } else if (s.charAt(i) == '8') {
                result += Math.pow(10, s.length() - 1 - i) * 8;
            } else if (s.charAt(i) == '9') {
                result += Math.pow(10, s.length() - 1 - i) * 9;
            }
        }
        return result;
    }
}
