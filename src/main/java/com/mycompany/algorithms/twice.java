/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.algorithms;

import java.util.Scanner;

/**
 *
 * @author admin
 */
public class twice {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int n = kb.nextInt(); // รับค่าinput
        int[] A = new int[n]; // สร้าง array A
        for (int i = 0; i < A.length; i++) { // วนรับค่า
            A[i] = kb.nextInt();
        }
        System.out.println(twicecheck(A)); // เรียก method เพื่อแสดงจำนวนที่ไม่ซ้ำ
    }

    public static int twicecheck(int A[]) { // 1, 2, 3, 3, 2, 1, 8
        int x = 0;
        for (int i = 1; i < A.length; i++) { 
            if (A[x] == A[i] && x != i ) { //ค่าเหมือนกัน
                A[x] = -1;      //ถ้าค่าเหมือนกัน set ให้เป็น -1
                A[i] = -1;      //ถ้าค่าเหมือนกัน set ให้เป็น -1
            }
            if (i == A.length - 1 && x != A.length - 1) { //ถ้า i วนครบ และ x ยังไม่ถึงตัวสุดท้าย
                i = -1;  //ให้เริ่ม i ใหม่
                x++; //เพิ่ม x เพื่อตรวจสอบตัวต่อไป
            }
        }
        for (int i = 0; i < A.length; i++) {
            if (A[i] != -1) {
                x=A[i];
            }
        }
        return x;
    }

}
